CFLAGS=

all: packager.o
	gcc -o bin/vsftpserver bin/package.o src/vsftpserver.c -lpthread $(CFLAGS)
	gcc -o bin/vsftpclient bin/package.o src/vsftpclient.c -lpthread $(CFLAGS)
	
packager.o:
	gcc -o bin/package.o -c src/packager.c  
    
clean: 
	rm -rf *.o
	rm -rf vsftpclient
	rm -rf vsftpserver
	