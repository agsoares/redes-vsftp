#ifndef PACKAGER_H
#define PACKAGER_H

#define WINDOW_SIZE 1

typedef struct slot_t slot_t;
struct slot_t {
  int used;
  char *data;
};




int package  (char *out, char *data);
int unpackage(char *in , char *data);

#endif /* PACKAGER_H */