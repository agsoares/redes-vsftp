/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include "../include/packager.h"

#define PORT 4000

int logging = 0;

void error(char *);

int main(int argc, char *argv[])
{
  int sock, length, n;
  char *filename;    
  struct sockaddr_in server, from;
  struct hostent *hp;
  char buffer[256];

  package("teste","teste2");
  

  if (argc > 3) {
    int i;   
    for (i = 1; i < argc; i++) {
      if (strcmp(argv[i], "-l") == 0) {
        printf("log\n");
        logging = 1;
      } else if (strcmp(argv[i], "-h") == 0) {
        i++;    
        printf("host:%s \n",argv[i]);
        hp = gethostbyname(argv[i]);
        if (hp==0) error("Unknown host");
      } else if (strcmp(argv[i], "-f") == 0) {
        i++;   
        printf("file:%s \n",argv[i]);    
        filename = argv[i];
      }
    }  
  } else {
    printf("Usage -l -h host -f filename\n");
    exit(0);
  }


  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) error("socket");

  server.sin_family = AF_INET;

  bcopy((char *)hp->h_addr, 
        (char *)&server.sin_addr,
        hp->h_length);
  server.sin_port = htons(atoi(argv[2]));
  length=sizeof(struct sockaddr_in);
  printf("Please enter the message: ");
  bzero(buffer,256);
  fgets(buffer,255,stdin);
  n=sendto(sock,buffer,
           strlen(buffer),0,&server,length);
  if (n < 0) error("Sendto");
  n = recvfrom(sock,buffer,256,0,&from, &length);
  if (n < 0) error("recvfrom");
  write(1, "Got an ack: ",12);
  write(1, buffer,n);
}

void error(char *msg)
{
  perror(msg);
  exit(0);
}